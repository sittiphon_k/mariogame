package platformer.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.Main;
import platformer.controller.PaneController.MenuPaneController;
import platformer.model.Character.Character;
import platformer.model.Direction;
import platformer.model.DropFromSky.Coin.Coin;
import platformer.model.DropFromSky.Item.*;
import platformer.model.DropFromSky.Meteor.Meteor;
import platformer.model.GameSound;
import platformer.model.Keys;
import platformer.model.Language;
import platformer.model.UI_Element.CountTime;
import platformer.model.UI_Element.LifeBar;
import platformer.model.UI_Element.Score;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public static final int GROUND = 300;

    private int money;
    private int time;
    private boolean gamePlayRunning = false;
    private ArrayList<String> unlockCostumeList;
    private boolean muteSound = false;
    private Language language;

    private Image platformImg;
    private ImageView backgroundImg;

    private boolean runFirstTime = true;
    private Character mainCharacter;
    private ArrayList<Coin> coinList;
    private Score score;
    private CountTime timeShow;
    private LifeBar lifeBar;
    private ArrayList<Item> items;
    private ArrayList<Meteor> meteors;

    private Keys keys;
    private GameSound groundSound;

    public Platform() throws IOException {
        money = 50;
        time = 30;
        coinList = new ArrayList<>();
        meteors = new ArrayList<>();
        items = new ArrayList<>();
        unlockCostumeList = new ArrayList<>();
        unlockCostumeList.add("10Sygr");
        unlockCostumeList.add("3Sbrb");
        unlockCostumeList.add("6Sgrw");
        keys = new Keys();
        language = Language.ENG;
        platformImg = new Image(getClass().getResourceAsStream("/img/background/backgroundNormal.png"));
        backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        mainCharacter = new Character(30, 30, 0, 0, KeyCode.A, KeyCode.D, KeyCode.W);
        score = new Score(30, GROUND + 30);
        ImageView backBtn = buildBackBtn();

        for (int i = 0; i < 3; i++) {
            coinList.add(new Coin());
        }
        meteors.add(new Meteor());
        buildAllItem();

        timeShow = new CountTime(this.time, 350, 15);
        lifeBar = new LifeBar(715, 10);

        setFallingEveryItem(false);
        setVisibleEveryNodeGamePlay(false);

        getChildren().add(backgroundImg);
        getChildren().addAll(coinList);
        getChildren().addAll(meteors);
        getChildren().addAll(items);
        getChildren().addAll(backBtn);
        getChildren().addAll(score);
        getChildren().addAll(timeShow);
        getChildren().addAll(lifeBar);
        getChildren().addAll(mainCharacter);
        addMenuPane();

        groundSound = new GameSound("/sound/GroundTheme.mp3");
        groundSound.playSound();
        runFirstTime = false;
    }

    public void startGame() {
        this.getChildren().remove(this.getChildren().size() - 1);
        resetGame();
        gamePlayRunning = true;
        setVisibleEveryNodeGamePlay(true);
        setFallingEveryItem(true);
    }

    public void gameOver() {
        if (gamePlayRunning) {
            gamePlayRunning = false;
            money = money + mainCharacter.getScore();
            buildGameOverPane();
        }
    }

    public void buildGameOverPane() {
        javafx.application.Platform.runLater(() -> {
            Pane gameOverPane = null;
            try {
                gameOverPane = FXMLLoader.load(getClass().getResource("/fxml/GameOverPane.fxml"));
            } catch (IOException e) {
            }
            this.getChildren().addAll(gameOverPane);
        });
    }

    public void replay() {
        this.getChildren().remove(this.getChildren().size() - 1);
        resetGame();
        gamePlayRunning = true;
    }

    public void resetGame() {
        Main.getTimerLoop().setCount(0);
        time = 30;
        mainCharacter.setLife(2);
        mainCharacter.setScore(0);
        mainCharacter.setDirection(Direction.NONE);
        mainCharacter.setX(mainCharacter.getStartX());
        mainCharacter.setY(mainCharacter.getStartY());
        mainCharacter.setFalling(true);
        setEveryItemToSky();
    }

    public void backToMenuPane() throws IOException, InterruptedException {
        if (!gamePlayRunning) {
            this.getChildren().remove(this.getChildren().size() - 1);
            Thread.sleep(30);
        }
        gamePlayRunning = false;
        setFallingEveryItem(false);
        setVisibleEveryNodeGamePlay(false);
        addMenuPane();
    }

    public void openSelectCostumePane() throws IOException {
        this.getChildren().remove(this.getChildren().size() - 1);
        Pane selectPane = FXMLLoader.load(getClass().getResource("/fxml/SelectCostumePane.fxml"));
        getChildren().addAll(selectPane);
    }

    public void openShopPane() throws IOException {
        this.getChildren().remove(this.getChildren().size() - 1);
        Pane selectPane = FXMLLoader.load(getClass().getResource("/fxml/ShopPane.fxml"));
        getChildren().addAll(selectPane);
    }

    public void openSettingPane() throws IOException {
        this.getChildren().remove(this.getChildren().size() - 1);
        Pane selectPane = FXMLLoader.load(getClass().getResource("/fxml/SettingPane.fxml"));
        getChildren().addAll(selectPane);
    }

    public void fallSomeItem() {
        if (gamePlayRunning) {
            int ran = new Random().nextInt(3) + 1;
            if (ran == 1) {
                items.get(0).setFalling(true);
                items.get(0).setVisible(true);
            } else if (ran == 2) {
                items.get(1).setFalling(true);
                items.get(1).setVisible(true);
            } else if (ran == 3) {
                items.get(2).setFalling(true);
                items.get(2).setVisible(true);
            }
        }
    }

    public void buildAllItem() {
        items.add(new TimeItem());
        items.add(new HeartItem());
        items.add(new StarItem());
    }

    public void reduceTime() throws InterruptedException {
        if (gamePlayRunning) {
            this.timeShow.setTimeText(time);
            if (time == 0) {
                gameOver();
            }
            if (time > 0) {
                this.time--;
            }
        }
    }

    public void setFallingEveryItem(boolean falling) {
        for (Coin coin : coinList) {
            coin.setFalling(falling);
        }
        for (Meteor meteor : meteors) {
            meteor.setFalling(falling);
        }
    }

    public void setVisibleEveryNodeGamePlay(boolean visit) {
        mainCharacter.setVisible(visit);
        score.setVisible(visit);
        timeShow.setVisible(visit);
        lifeBar.setVisible(visit);
        for (Coin coin : coinList) {
            coin.setVisible(visit);
        }
        for (Meteor meteor : meteors) {
            meteor.setVisible(visit);
        }
    }

    public void setEveryItemToSky() {
        for (Coin coin : coinList) {
            coin.toSky();
        }
        for (Item item : items) {
            item.toSky();
        }
        for (Meteor meteor : meteors) {
            meteor.toSky();
        }
    }

    public void addMenuPane() throws IOException {
        Pane menu = FXMLLoader.load(getClass().getResource("/fxml/MenuPane.fxml"));
        getChildren().addAll(menu);
        if (!runFirstTime) {
            MenuPaneController.refreshPaneLanguage();
        }
    }

    public void changeCostume(String imageID) throws IOException, InterruptedException {
        mainCharacter.getChildren().clear();
        mainCharacter.buildCharacter(imageID);
        Thread.sleep(50);
        backToMenuPane();
    }

    public ImageView buildBackBtn() {
        Image backImg = new Image(getClass().getResourceAsStream("/img/icon/back.png"));
        ImageView backBtn = new ImageView();
        backBtn.setImage(backImg);
        backBtn.setFitHeight(40);
        backBtn.setFitWidth(40);
        backBtn.setLayoutX(20);
        backBtn.setLayoutY(20);
        backBtn.setCursor(Cursor.HAND);
        backBtn.setOnMouseClicked(event -> {
            try {
                backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });
        return backBtn;
    }

    public void addUnlockCostume(String id) {
        unlockCostumeList.add(id);
    }

    public Character getCharacter() {
        return mainCharacter;
    }

    public Keys getKeys() {
        return keys;
    }

    public Score getScore() {
        return score;
    }

    public ArrayList<Coin> getCoinList() {
        return coinList;
    }

    public CountTime getTimeShow() {
        return timeShow;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public ArrayList<Meteor> getMeteors() {
        return meteors;
    }

    public LifeBar getLifeBar() {
        return lifeBar;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isGamePlayRunning() {
        return gamePlayRunning;
    }

    public void setGamePlayRunning(boolean gamePlayRunning) {
        this.gamePlayRunning = gamePlayRunning;
    }

    public ArrayList<String> getUnlockCostumeList() {
        return unlockCostumeList;
    }

    public void setUnlockCostumeList(ArrayList<String> unlockCostumeList) {
        this.unlockCostumeList = unlockCostumeList;
    }

    public boolean isMuteSound() {
        return muteSound;
    }

    public void setMuteSound(boolean muteSound) {
        if (muteSound) {
            groundSound.muteSound();
        } else {
            groundSound.unmuteSound();
        }
        this.muteSound = muteSound;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public boolean isRunFirstTime() {
        return runFirstTime;
    }

    public void setRunFirstTime(boolean runFirstTime) {
        this.runFirstTime = runFirstTime;
    }

    public void stopSound() {
        groundSound.stopSound();
    }
}

