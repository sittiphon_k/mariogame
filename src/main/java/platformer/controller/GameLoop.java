package platformer.controller;

import platformer.model.Character.Character;
import platformer.model.UI_Element.LifeBar;
import platformer.model.UI_Element.Score;
import platformer.view.Platform;

public class GameLoop implements Runnable {
    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;
    private float refreshRate = 1000.0f / 60;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 10;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void updateScore(Score score, Character character) {
        javafx.application.Platform.runLater(() -> {
            score.setPoint(character.getScore());
        });
    }

    private void updateLife(LifeBar life, Character character) {
        javafx.application.Platform.runLater(() -> {
            life.setLifeScore(character.getLife());
        });
    }

    private void update(Character character) {
        if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
            character.getImageView().tick();
        }
        if (platform.getKeys().isPressed(character.getLeftKey())) {
            character.setScaleX(-1);
            character.moveLeft();
        }
        if (platform.getKeys().isPressed(character.getRightKey())) {
            character.setScaleX(1);
            character.moveRight();
        }
        if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey())) {
            character.stop();
        }
        if (platform.getKeys().isPressed(character.getUpKey())) {
            character.jump();
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                Thread.sleep((long) refreshRate);
            } catch (InterruptedException e) {
            }
            if (platform.isGamePlayRunning()) {
                try {
                    Thread.sleep((long) (interval - refreshRate));
                } catch (InterruptedException e) {
                }
                update(platform.getCharacter());
                updateScore(platform.getScore(), platform.getCharacter());
                updateLife(platform.getLifeBar(), platform.getCharacter());
            }
        }
    }
}
