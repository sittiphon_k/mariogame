package platformer.controller.PaneController;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import platformer.Main;
import platformer.model.Language;

import java.io.IOException;

public class MenuPaneController {
    private static Label playLabelStatic;
    private static Label costumeLabelStatic;
    private static Label shopLabelStatic;
    private static Label settingLabelStatic;
    private static Label exitLabelStatic;
    public String linke;

    @FXML
    private ImageView playBtn;

    @FXML
    private ImageView costumeBtn;

    @FXML
    private ImageView shopBtn;

    @FXML
    private ImageView settingBtn;

    @FXML
    private ImageView exitBtn;

    @FXML
    private Label playLabel;

    @FXML
    private Label costumeLabel;

    @FXML
    private Label shopLabel;

    @FXML
    private Label settingLabel;

    @FXML
    private Label exitLabel;

    @FXML
    public void initialize() {
        playLabelStatic = playLabel;
        costumeLabelStatic = costumeLabel;
        shopLabelStatic = shopLabel;
        settingLabelStatic = settingLabel;
        exitLabelStatic = exitLabel;

        playBtn.setOnMouseClicked(event -> {
            play();
            Main.getPlatform().startGame();
        });

        costumeBtn.setOnMouseClicked(event -> {
            cost();
            try {
                Main.getPlatform().openSelectCostumePane();
            } catch (IOException e) {
            }
        });

        shopBtn.setOnMouseClicked(event -> {
            shop();
            try {
                Main.getPlatform().openShopPane();
            } catch (IOException e) {
            }
        });

        settingBtn.setOnMouseClicked(event -> {
            setting();
            try {
                Main.getPlatform().openSettingPane();
            } catch (IOException e) {
            }
        });

        exitBtn.setOnMouseClicked(event -> {
            exit();
            System.exit(0);
        });
    }

    public static void refreshPaneLanguage() {
        if (Main.getPlatform().getLanguage() == Language.ENG) {
            playLabelStatic.setText("PLAY");
            costumeLabelStatic.setText("COSTUME");
            shopLabelStatic.setText("SHOP");
            settingLabelStatic.setText("SETTING");
            exitLabelStatic.setText("EXIT");
        } else if (Main.getPlatform().getLanguage() == Language.TH) {
            playLabelStatic.setText("เริ่มเกม");
            costumeLabelStatic.setText("ชุดตัวละคร");
            shopLabelStatic.setText("ร้านค้า");
            settingLabelStatic.setText("ตั้งค่า");
            exitLabelStatic.setText("จบเกม");
        } else {
            playLabelStatic.setText("开始游戏");
            costumeLabelStatic.setText("服装");
            shopLabelStatic.setText("商店");
            settingLabelStatic.setText("设置");
            exitLabelStatic.setText("退出");
        }
    }

    public void play() {
        setLinke("play");
    }

    public void cost() {
        setLinke("select");
    }

    public void shop() {
        setLinke("shop");
    }

    public void setting() {
        setLinke("setting");
    }

    public void exit() {
        setLinke("exit");
    }

    public String getLinke() {
        return linke;
    }

    public void setLinke(String linke) {
        this.linke = linke;
    }
}