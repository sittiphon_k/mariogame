package platformer.controller.PaneController;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import platformer.Main;
import platformer.model.Language;

import java.io.IOException;

public class GameOverPaneController {
    @FXML
    private Label gameOverLabel;

    @FXML
    Button playAgainBtn;

    @FXML
    Button menuBtn;

    @FXML
    public void initialize() {
        playAgainBtn.setOnMouseClicked(event -> {
            Main.getPlatform().replay();
        });

        menuBtn.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });

        refreshPaneLanguage();
    }

    public void refreshPaneLanguage() {
        if (Main.getPlatform().getLanguage() == Language.ENG) {
            gameOverLabel.setText("GAME OVER");
            playAgainBtn.setText("Play again");
            menuBtn.setText("Main menu");
        } else if (Main.getPlatform().getLanguage() == Language.TH) {
            gameOverLabel.setText("จบเกม");
            playAgainBtn.setText("เล่นอีกครั้ง");
            menuBtn.setText("กลับเมนู");
        } else {
            gameOverLabel.setText("游戏结束");
            playAgainBtn.setText("再玩一次");
            menuBtn.setText("主页面");
        }
    }
}