package platformer.controller.PaneController;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import platformer.Main;
import platformer.model.Language;

public class ChoseLangPaneController {
    @FXML
    private ImageView backBtn;

    @FXML
    private ImageView engBtn;

    @FXML
    private ImageView thBtn;

    @FXML
    private ImageView chBtn;

    @FXML
    public void initialize(){
        backBtn.setOnMouseClicked(event -> {
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });

        engBtn.setOnMouseClicked(event -> {
            Main.getPlatform().setLanguage(Language.ENG);
            SettingPaneController.refreshPaneLanguage();
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });

        thBtn.setOnMouseClicked(event -> {
            Main.getPlatform().setLanguage(Language.TH);
            SettingPaneController.refreshPaneLanguage();
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });

        chBtn.setOnMouseClicked(event -> {
            Main.getPlatform().setLanguage(Language.CH);
            SettingPaneController.refreshPaneLanguage();
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });
    }
}