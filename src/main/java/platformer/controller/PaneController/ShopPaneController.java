package platformer.controller.PaneController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import platformer.Main;
import platformer.model.Costume.CostumeHBox;
import platformer.model.Language;

import java.io.IOException;
import java.util.ArrayList;

public class ShopPaneController {
    private ArrayList<CostumeHBox> costumeList;
    private static Label moneyLabelStatic;

    @FXML
    private Label titleLabel;

    @FXML
    private Button closeBtn;

    @FXML
    private ImageView background;

    @FXML
    private AnchorPane showCostume;

    @FXML
    public AnchorPane allCostumePane;

    @FXML
    private Label moneyLabel;

    @FXML
    public void initialize() {
        moneyLabelStatic = this.moneyLabel;

        costumeList = BuildCostumeCollection.getAllContume();
        checkCostumeUnlock();
        for (CostumeHBox costumeHBox : costumeList) {
            setCostumeClicked(costumeHBox);
        }
        allCostumePane.getChildren().addAll(costumeList);

        moneyLabel.setText(Integer.toString(Main.getPlatform().getMoney()));

        closeBtn.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });

        background.setOnMouseClicked(event -> {
            try {
                Main.getPlatform().backToMenuPane();
            } catch (IOException | InterruptedException e) {
            }
        });

        refreshPaneLanguage();
    }

    public void setCostumeClicked(CostumeHBox box) {
        box.setOnMouseClicked(event -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BuyPane.fxml"));

            Pane pane = null;
            try {
                pane = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            BuyPaneController controller = loader.getController();
            controller.setName(box.getCostumeName());
            controller.setPrice(box.getCostumePrice());
            controller.setContent(box.getCostumeDetail());
            controller.setId(box.getCostumeId());
            controller.setBought(box.isUnlock());
            controller.initialize();

            Main.getPlatform().getChildren().addAll(pane);
        });
    }

    public static void refreshMoney() {
        moneyLabelStatic.setText(Integer.toString(Main.getPlatform().getMoney()));
    }

    public void checkCostumeUnlock() {
        for (CostumeHBox costume : costumeList) {
            for (String unlockCostume : Main.getPlatform().getUnlockCostumeList()) {
                if (costume.getCostumeId().equals(unlockCostume)) {
                    costume.setUnlock(true);
                }
            }
        }
    }

    public void refreshPaneLanguage() {
        if (Main.getPlatform().getLanguage() == Language.ENG) {
            titleLabel.setText("SHOP");
        } else if (Main.getPlatform().getLanguage() == Language.TH) {
            titleLabel.setText("ร้านค้า");
        } else {
            titleLabel.setText("商店");
        }
    }
}