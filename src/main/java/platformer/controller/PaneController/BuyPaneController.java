package platformer.controller.PaneController;

import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.util.Duration;
import platformer.Main;
import platformer.model.Costume.CostumeHBox;
import platformer.model.Language;
import platformer.view.Platform;

public class BuyPaneController {
    private String name = "";
    private String content = "";
    private int price = 0;
    private String id = "1Sgb";
    private boolean bought = false;

    private String infoTitle;
    private String infoNotEnoughMoney;
    private String infoCannotBuy;
    private String infoBought;


    @FXML
    private Button closeBtn;

    @FXML
    private ImageView background;

    @FXML
    private Label costumeNameLabel;

    @FXML
    private ImageView costumeExampleImage;

    @FXML
    private Pane detailPane;

    @FXML
    private Label priceLabel;

    @FXML
    private Button buyBtn;

    @FXML
    public void initialize() {
        final Text text = new Text(10, 20, "");
        text.setStyle("-fx-font: 16 System;");

        final Animation animation = new Transition() {
            {
                setCycleDuration(Duration.millis(2000));
            }

            protected void interpolate(double frac) {
                final int length = getContent().length();
                final int n = Math.round(length * (float) frac);
                text.setText(getContent().substring(0, n));
            }

        };

        priceLabel.setText(Integer.toString(price));
        costumeNameLabel.setText(name);
        animation.play();
        detailPane.getChildren().add(text);
        Image image = new Image(getClass().getResourceAsStream("/img/character/singleCostume/" + id + ".png"));
        costumeExampleImage.setImage(image);

        buyBtn.setOnMouseClicked(event -> {
            if (bought) {
                alertInfo(infoCannotBuy);
            } else if (Main.getPlatform().getMoney() < price) {
                alertInfo(infoNotEnoughMoney);
            } else {
                alertInfo(infoBought);
                Main.getPlatform().addUnlockCostume(id);
                Main.getPlatform().setMoney(Main.getPlatform().getMoney() - price);
                ShopPaneController.refreshMoney();
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });

        background.setOnMouseClicked(event -> {
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });

        closeBtn.setOnMouseClicked(event -> {
            Main.getPlatform().getChildren().remove(Main.getPlatform().getChildren().size() - 1);
        });

        refreshPaneLanguage();
    }

    public void alertInfo(String info) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(infoTitle);
        alert.setHeaderText(null);
        alert.setContentText(info);
        alert.showAndWait();
    }

    public void refreshPaneLanguage() {
        if (Main.getPlatform().getLanguage() == Language.ENG) {
            buyBtn.setText("BUY");
            infoTitle = "Information";
            infoBought = "Congratulations you have a new costume !";
            infoCannotBuy = "You already have it.";
            infoNotEnoughMoney = "Your money is not enough.";
        } else if (Main.getPlatform().getLanguage() == Language.TH) {
            buyBtn.setText("ซื้อ");
            infoTitle = "แจ้งเตือน";
            infoBought = "ซื้อสินค้าสำเร็จ ยินดีด้วย!!";
            infoCannotBuy = "มีชุดนี้แล้ว";
            infoNotEnoughMoney = "จำนวนเงินขิงคุณไม่พอ";
        } else {
            buyBtn.setText("购买");
            infoTitle = "消息提示";
            infoBought = "恭喜你成功购买一件新服装";
            infoCannotBuy = "你已经购买过了";
            infoNotEnoughMoney = "金币不足";
        }
    }

    public void buy(Platform p, CostumeHBox c) {
        if (c.isUnlock() == false) {
            p.setMoney(p.getMoney() - c.getCostumePrice());
            c.setUnlock(true);
        }
        if (c.isUnlock()) {
            p.setMoney(p.getMoney());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isBought() {
        return bought;
    }

    public void setBought(boolean bought) {
        this.bought = bought;
    }
}