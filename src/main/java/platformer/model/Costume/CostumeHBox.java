package platformer.model.Costume;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

public class CostumeHBox extends HBox {
    private final int BOX_HEIGHT = 100;
    private final int BOX_WIDTH = 80;

    private final int IMAGE_HEIGHT = 80;
    private final int IMAGE_WIDTH = 60;

    private String imgPath;
    private String costumeName;
    private int costumePrice;
    private String costumeDetail;
    private String costumeId;
    private boolean unlock;

    public CostumeHBox(String name, int price, String detail, String imgPath, String id, double leftAnchor, String position) {
        this.costumeName = name;
        this.costumePrice = price;
        this.costumeDetail = detail;
        this.imgPath = imgPath;
        this.costumeId = id;
        this.unlock = false;

        Image cos1 = new Image(getClass().getResourceAsStream(imgPath));
        ImageView imgCostume = new ImageView(cos1);
        imgCostume.setFitHeight(IMAGE_HEIGHT);
        imgCostume.setFitWidth(IMAGE_WIDTH);

        this.setPrefHeight(BOX_HEIGHT);
        this.setPrefWidth(BOX_WIDTH);
        this.setStyle("-fx-background-color: #E6E6FA ; -fx-background-radius:10; -fx-border-color: #B0C4DE; -fx-border-radius: 10");
        this.setPadding(new Insets(10, 10, 10, 10));
        this.setLayoutX(100);
        this.setLayoutY(100);
        if (position.toUpperCase().equals("TOP")) {
            AnchorPane.setTopAnchor(this, 5.0);
        } else if (position.toUpperCase().equals("BOTTOM")) {
            AnchorPane.setBottomAnchor(this, 5.0);
        }
        AnchorPane.setLeftAnchor(this, leftAnchor);
        this.getChildren().add(imgCostume);
    }

    public String getImgPath() {
        return imgPath;
    }

    public String getCostumeId() {
        return costumeId;
    }

    public void setCostumeId(String costumeId) {
        this.costumeId = costumeId;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public String getCostumeName() {
        return costumeName;
    }

    public void setCostumeName(String costumeName) {
        this.costumeName = costumeName;
    }

    public int getCostumePrice() {
        return costumePrice;
    }

    public void setCostumePrice(int costumePrice) {
        this.costumePrice = costumePrice;
    }

    public String getCostumeDetail() {
        return costumeDetail;
    }

    public void setCostumeDetail(String costumeDetail) {
        this.costumeDetail = costumeDetail;
    }
}