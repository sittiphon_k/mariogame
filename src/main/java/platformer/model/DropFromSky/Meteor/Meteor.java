package platformer.model.DropFromSky.Meteor;

import javafx.scene.image.Image;
import platformer.model.AnimatedSprite;
import platformer.model.DropFromSky.DropFromSky;
import platformer.view.Platform;

public class Meteor extends DropFromSky {
    public final static int METEOR_HEIGHT = 70;
    public final static int METEOR_WIDTH = 44;

    private AnimatedSprite imageView;

    public Meteor() {
        toSky();
        setTranslateY(y);
        setTranslateX(x);
        Image meteorImg = new Image(getClass().getResourceAsStream("/img/item/meteor.png"));
        this.imageView = new AnimatedSprite(meteorImg, 5, 5, 0, 0, 384, 638);
        this.imageView.setFitHeight(METEOR_HEIGHT);
        this.imageView.setFitWidth(METEOR_WIDTH);
        this.getChildren().addAll(this.imageView);
    }

    public Meteor(int x, int y) {
        this.x = x;
        this.y = y;
        setTranslateY(y);
        setTranslateX(x);
        Image meteorImg = new Image(getClass().getResourceAsStream("/img/item/meteor.png"));
        this.imageView = new AnimatedSprite(meteorImg, 5, 5, 0, 0, 384, 638);
        this.imageView.setFitHeight(METEOR_HEIGHT);
        this.imageView.setFitWidth(METEOR_WIDTH);
        this.getChildren().addAll(this.imageView);
    }

    @Override
    public void checkReachFloor() {
        if (falling && y >= Platform.GROUND - METEOR_HEIGHT) {
            toSky();
        }
    }

    public AnimatedSprite getImageView() {
        return imageView;
    }
}