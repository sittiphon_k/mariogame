package platformer.model.DropFromSky;

import javafx.scene.layout.Pane;
import platformer.view.Platform;

import java.util.Random;

public abstract class DropFromSky extends Pane {
    protected int x;
    protected int y;

    protected int yVelocity;
    protected boolean falling = true;

    public void moveY() {
        setTranslateY(y);

        if (falling) {
            y = y + yVelocity;
        }
    }

    public void toSky() {
        y = randomY();
        x = randomX();
        yVelocity = random_yVelocity();
        setTranslateX(x);
    }

    public void repaint() {
        moveY();
    }

    public void collected() {
        toSky();
    }

    public int randomX() {
        return new Random().nextInt(Platform.WIDTH - 20) + 1;
    }

    public int randomY() {
        return -new Random().nextInt(100) + 15;
    }

    public int random_yVelocity() {
        return new Random().nextInt(5) + 1;
    }

    public abstract void checkReachFloor();

    public void setFalling(boolean falling) {
        this.falling = falling;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}