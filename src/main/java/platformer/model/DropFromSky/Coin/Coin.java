package platformer.model.DropFromSky.Coin;

import javafx.scene.image.Image;
import platformer.model.AnimatedSprite;
import platformer.model.DropFromSky.DropFromSky;
import platformer.view.Platform;

public class Coin extends DropFromSky {
    public static final int COIN_WIDTH = 20;
    public static final int COIN_HEIGHT = 30;
    private AnimatedSprite imageView;

    public Coin() {
        toSky();
        setTranslateY(y);
        setTranslateX(x);
        Image coinImg = new Image(getClass().getResourceAsStream("/img/coin/coin_spriteSheet.png"));
        this.imageView = new AnimatedSprite(coinImg, 10, 10, 0, 0, 83, 80);
        this.imageView.setFitHeight(COIN_HEIGHT);
        this.imageView.setFitWidth(COIN_WIDTH);
        this.getChildren().addAll(this.imageView);
    }

    public Coin(int x, int y) {
        this.x = x;
        this.y = y;
        setTranslateY(y);
        setTranslateX(x);
        Image coinImg = new Image(getClass().getResourceAsStream("/img/coin/coin_spriteSheet.png"));
        this.imageView = new AnimatedSprite(coinImg, 10, 10, 0, 0, 83, 80);
        this.imageView.setFitHeight(COIN_HEIGHT);
        this.imageView.setFitWidth(COIN_WIDTH);
        this.getChildren().addAll(this.imageView);
    }

    @Override
    public void checkReachFloor() {
        if (falling && y >= Platform.GROUND - COIN_HEIGHT + 5) {
            toSky();
        }
    }

    // GETTER SETTER
    public AnimatedSprite getImageView() {
        return imageView;
    }

    public boolean isFalling() {
        return falling;
    }

    public void setFalling(boolean falling) {
        this.falling = falling;
    }
}