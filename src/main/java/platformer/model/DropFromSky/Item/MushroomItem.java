package platformer.model.DropFromSky.Item;

import java.util.Random;

public class MushroomItem extends Item {
    public MushroomItem() {
        super("/img/item/Mushroom.png", "Mushroom", ItemType.SPEED);
    }

    @Override
    public int getStat() {
        return new Random().nextInt(5) + 5;
    }
}