package platformer.model.DropFromSky.Item;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import platformer.model.DropFromSky.DropFromSky;
import platformer.view.Platform;

public class Item extends DropFromSky {
    public final static int ITEM_HEIGHT = 30;
    public final static int ITEM_WIDTH = 30;

    protected String itemName;
    protected ItemType itemType;
    protected ImageView imageView;

    public Item(String imagePath, String name, ItemType type) {
        this.itemName = name.toUpperCase();
        this.itemType = type;
        falling = false;
        toSky();
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.imageView = new ImageView();
        this.imageView.setImage(new Image(getClass().getResourceAsStream(imagePath)));
        this.imageView.setFitWidth(ITEM_WIDTH);
        this.imageView.setFitHeight(ITEM_HEIGHT);
        this.getChildren().addAll(imageView);
        this.setVisible(false);
    }

    public Item(int x, int y, String imagePath, String name, ItemType type) {
        this.itemName = name.toUpperCase();
        this.itemType = type;
        falling = false;
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.imageView = new ImageView();
        this.imageView.setImage(new Image(getClass().getResourceAsStream(imagePath)));
        this.imageView.setFitWidth(ITEM_WIDTH);
        this.imageView.setFitHeight(ITEM_HEIGHT);
        this.getChildren().addAll(imageView);
        this.setVisible(false);
    }

    @Override
    public void checkReachFloor() {
        if (falling && y >= Platform.GROUND - ITEM_HEIGHT) {
            toSky();
            falling = false;
            this.setVisible(false);
        }
    }

    @Override
    public void collected() {
        toSky();
        falling = false;
        setVisible(false);
    }

    public ItemType getItemType() {
        return itemType;
    }

    public int getStat() {
        return 0;
    }

    public String getItemName() {
        return itemName;
    }
}