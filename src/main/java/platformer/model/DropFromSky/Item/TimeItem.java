package platformer.model.DropFromSky.Item;

import java.util.Random;

public class TimeItem extends Item {
    public TimeItem() {
        super("/img/item/Time.png", "Time", ItemType.TIME);
    }

    public TimeItem(int x, int y) {
        super(x, y, "/img/item/Time.png", "Time", ItemType.TIME);
    }

    @Override
    public int getStat() {
        return new Random().nextInt(20) + 10;
    }
}