package platformer.model;

public enum Language {
    TH, CH, ENG
}