package platformer.model.UI_Element;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class CountTime extends Pane {
    private int time;
    private Label timeShow;

    public CountTime(int time, int x, int y) {
        this.time = time;
        timeShow = new Label("Time: " + time);
        setTranslateX(x);
        setTranslateY(y);
        timeShow.setFont(Font.font("Verdana", FontWeight.BOLD, 25));
        timeShow.setTextFill(Color.web("#FFF"));
        getChildren().addAll(timeShow);
    }

    public void increaseTimeBy(int plus) {
        this.time = time + plus;
        setTimeText(this.time);
    }

    public void decreaseTimeBy(int minus) {
        this.time = time - minus;
        setTimeText(this.time);
    }

    public void setTimeText(int time) {
        this.timeShow.setText("Time: " + Integer.toString(time));
    }

    public int getTime() {
        return this.time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}