package platformer.model;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class GameSound {
    private Media song;
    private MediaPlayer player;

    public GameSound(String path) {
        song = new Media(getClass().getResource(path).toString());
        player = new MediaPlayer(song);
    }

    public void playSound() {
        player.play();
    }

    public void unmuteSound() {
        player.setMute(false);
    }

    public void muteSound() {
        player.setMute(true);
    }

    public void stopSound() {
        player.stop();
    }
}